package truco;

import elementos.Rank;

public class PontuacaoTruco {
	public static int getValor(Rank rank) {
		for(PontoRank pontoRank: PontoRank.values()) {
			if(pontoRank.rank == rank) {
				return pontoRank.valor;
			}
		}
		
		return -1;
	}
	public enum PontoRank{
		As(Rank.As, 8),
		Dois(Rank.Dois, 9),
		Tres(Rank.Tres, 10),
		Quatro(Rank.Quatro, 1),
		Cinco(Rank.Cinco, 2),
		Seis(Rank.Seis, 3),
		Sete(Rank.Sete, 4),
		Dama(Rank.Dama, 5),
		Valete(Rank.Valete, 6),
		Rei(Rank.Rei, 7);
		
		private Rank rank;
		private int valor;
		
		private PontoRank(Rank rank, int valor) {
			this.rank = rank;
			this.valor = valor;
		}
	}
}
