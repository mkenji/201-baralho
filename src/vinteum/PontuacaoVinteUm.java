package vinteum;

import elementos.Rank;

public class PontuacaoVinteUm {
	
	public static int getValor(Rank rank) {
		for(PontoRank pontoRank: PontoRank.values()) {
			if(pontoRank.rank == rank) {
				return pontoRank.valor;
			}
		}
		
		return -1;
	}
	
	public enum PontoRank{
		As(Rank.As, 1),
		Dois(Rank.Dois, 2),
		Tres(Rank.Tres, 3),
		Quatro(Rank.Quatro, 4),
		Cinco(Rank.Cinco, 5),
		Seis(Rank.Seis, 6),
		Sete(Rank.Sete, 7),
		Oito(Rank.Oito, 8),
		Nove(Rank.Nove, 9),
		Dez(Rank.Dez, 10),
		Valete(Rank.Valete, 10),
		Dama(Rank.Dama, 10),
		Rei(Rank.Rei, 10);
		
		private Rank rank;
		private int valor;
		
		private PontoRank(Rank rank, int valor) {
			this.rank = rank;
			this.valor = valor;
		}
	}
}
